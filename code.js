var fs = require('fs');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

var linecount = 0;
var inputdatalength;
var inputcheckcount;
var check2;
var testdata=[];
var dataset=[];

readline.on('line', function (line,) {
    linecount++;
    if (linecount == 1) {
        // console.log("1",linecount,line);
        inputdatalength = parseInt(line) + 1;
        check2=inputdatalength+1;
    } else if (linecount>1 && linecount<=inputdatalength) {
        // console.log("2",linecount,line);
        processdata(line);
    } else if (linecount === check2) {
        // console.log("3",linecount,line);
        inputcheckcount = inputdatalength+1+parseInt(line);
    } else if (linecount > check2 && linecount<=inputcheckcount) {
        // console.log("4",linecount,line);
        processtest(line);
    }
})
.on('close', function(line) {
    bestrange(dataset,testdata,inputdatalength,inputcheckcount);
   });

function processdata(data){
    let data_1=data.split(" ");
    let processdata={};
    processdata['price']=parseInt(data_1[0]);
    processdata['rating']=parseInt(data_1[1]);
    dataset.push(processdata);
    return;
}

function processtest(data){
    let data_1=data.split(" ");
    let processdata=[];
    for(let i=0;i<data_1.length;i++){
        processdata.push(parseInt(data_1[i])); 
    }
    testdata.push(processdata);
    return;
}

function bestrange(data_set,range_data,x,y){
    console.log("data_set",data_set,range_data,x,y)
    fs.truncate('./output.txt', 0, function(){console.log('done')})
 for(let i=0;i<range_data.length;i++){
     let startPrice=range_data[i][0];
     let endPrice=range_data[i][1];
     var maxconfig=0;
     for(let j=0;j<data_set.length;j++){
         if(data_set[j].price>=startPrice && data_set[j].price<=endPrice && data_set[j].rating>maxconfig){
            maxconfig=data_set[j].rating;
         }
     }
     console.log(maxconfig);
     fs.appendFile('./output.txt', maxconfig+"\n", function (err) {
        if (err) {
          console.log("some error occurred");
        } 
      })
 }
}